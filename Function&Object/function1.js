const add = (a, b) => {
    return a + b;
}

const sub = (a, b) => {
    return a - b;
}

const mul = (a, b) => {
    return a * b;
}

const div = (a, b) => {
    if (b === 0) {
        console.error("Error")
        return;
    }
    return a / b;
}

function calculate(num1, num2, callback) {
    console.log(callback(num1,num2))
}

function isNumber(op1, op2) {
    if (!isNaN(op1) || !isNaN(op2)) {
        return op1||op2;
    } else {
        op1 = Number(prompt("value1"))
        op2 = Number(prompt("value2"))
        return;
    }
}

const op1 = (parseFloat(prompt("value1"))),
    op2 = (parseFloat(prompt("value2"))),
    sign = prompt("sign");

switch (sign) {
    case "+": calculate(isNumber(op1), isNumber(op2), add)
        break;
    case "-": calculate(op1, op2, sub)
        break;
    case "*": calculate(op1, op2, mul)
        break;
    case "/": calculate(op1, op2, div)
        break;
}

